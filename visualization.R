library(tidyverse)
pdf("visualization.pdf")
dt<- read.csv("Diabetes.csv")
dt

x <- dt$Glucose
y <- dt$BloodPressure

plot(x,y,xlab = "Glucose" ,ylab = "Blood Pressure",
     main = "Glucose vs Blood Pressure",col = 'steelblue1',
     col.lab = 'brown4',col.main = 'red2',)
abline(lm(y~x,data = dt),col = 'red')
cor(x,y,method = "pearson")


g <- hist(y , main= "Frequency of Blood Presssure",
          xlab = "Blood Pressure",ylab = "Frequency",col='azure',col.main='red3',
          col.lab = 'magenta4')
mn <- mean(y)
mn
stdD <- sd(y)
stdD
x1 <- seq(0,120,2)
y1 <- dnorm(x1,mean = mn,sd= stdD)

y1 <- y1 * diff(g$mids[1:2]) * length(y)
lines(x1,y1,col = 'blue')
getwd()




